package com.kenfogel.fish_form_validated_demo.controllers;

import com.kenfogel.fish_form_validated_demo.controls.DoubleTextField;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.kenfogel.fish_form_validated_demo.beans.FishData;
import com.kenfogel.fish_form_validated_demo.persistence.FishV2DAO;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

/**
 * FXML controller for FishForm
 *
 * @author Ken Fogel
 */
public class FishFormController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(FishFormController.class);
    
    private FishV2DAO fishDAO;
    private final FishData fishData;

    @FXML
    private ResourceBundle resources;

    @FXML
    private DoubleTextField idTextField;

    @FXML
    private TextField commonNameTextField;

    @FXML
    private TextField latinTextField;

    @FXML
    private TextField phTextField;

    @FXML
    private TextField khTextField;

    @FXML
    private TextField tempTextField;

    @FXML
    private TextField fishSizeTextField;

    @FXML
    private TextField speciesOriginTextField;

    @FXML
    private TextField tankSizeTextField;

    @FXML
    private TextField stockingTextField;

    @FXML
    private TextField dietTextField;

    /**
     * Default constructor creates an instance of FishData that can be bound to
     * the form
     */
    public FishFormController() {
        super();
        fishData = new FishData();
    }

    /**
     * This method is automatically called after the fxml file has been loaded.
     * This code binds the properties of the data bean to the JavaFX controls.
     * Changes to a control is immediately written to the bean and a change to
     * the bean is immediately shown in the control.
     */
    @FXML
    private void initialize() {
        Bindings.bindBidirectional(idTextField.textProperty(), fishData.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(commonNameTextField.textProperty(), fishData.commonNameProperty());
        Bindings.bindBidirectional(latinTextField.textProperty(), fishData.latinProperty());
        Bindings.bindBidirectional(phTextField.textProperty(), fishData.phProperty());
        Bindings.bindBidirectional(khTextField.textProperty(), fishData.khProperty());
        Bindings.bindBidirectional(tempTextField.textProperty(), fishData.tempProperty());
        Bindings.bindBidirectional(fishSizeTextField.textProperty(), fishData.fishSizeProperty());
        Bindings.bindBidirectional(speciesOriginTextField.textProperty(), fishData.speciesOriginProperty());
        Bindings.bindBidirectional(tankSizeTextField.textProperty(), fishData.tankSizeProperty());
        Bindings.bindBidirectional(stockingTextField.textProperty(), fishData.stockingProperty());
        Bindings.bindBidirectional(dietTextField.textProperty(), fishData.dietProperty());
//        idTextField.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!validate(newValue)) {
//                newValue = oldValue;
//            }
//        });

    }

    private boolean validate(String text) {
        String numberRegEx = "\\b((-1)|([1-9][0-9]{0,2}))\\b";
        return ("".equals(text) || text.matches(numberRegEx));
    }

    /**
     * Exit event handler
     *
     * @param event
     */
    @FXML
    void exitFish(ActionEvent event) {
        LOG.info("Program exit");
        Platform.exit();
    }

    /**
     * Advance to the next fish in the table
     *
     * @param event
     * @throws SQLException
     */
    @FXML
    void nextFish(ActionEvent event) throws SQLException {
        fishDAO.findNextByID(fishData);
    }

    /**
     * Move to the previous record in the table
     *
     * @param event
     * @throws SQLException
     */
    @FXML
    void prevFish(ActionEvent event) throws SQLException {
        fishDAO.findPrevByID(fishData);
    }

    /**
     * Sets a reference to the FishDAO object that retrieves data from the
     * database.
     *
     * @param fishDAO
     * @throws SQLException
     */
    public void setFishDAO(FishV2DAO fishDAO) throws SQLException {
        this.fishDAO = fishDAO;
        fishDAO.findNextByID(fishData);
    }

}
