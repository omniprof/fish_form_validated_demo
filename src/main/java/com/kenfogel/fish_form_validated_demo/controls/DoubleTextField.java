package com.kenfogel.fish_form_validated_demo.controls;

import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ken
 */
public class DoubleTextField extends TextField {

    //http://gamon.webfactional.com/regexnumericrangegenerator/
    String numberRegEx = "\\b((-1)|([1-9][0-9]{0,2}))\\b";
    private final static Logger LOG = LoggerFactory.getLogger(DoubleTextField.class);

    @Override
    public void replaceText(int start, int end, String text) {
        LOG.debug("replaceText: " + text + "  start: " + start + "  end: " + end);
        // Retrieve original string in the event that the new string is 
        // invalid and the old string must be restored
        String oldValue = getText();
        boolean isANum = false;

        // Loop through the new text
        for (int x = 0; x < text.length(); ++x) {
            if (start == 0) { // first character
                if (Character.isDigit(text.charAt(x)) || text.charAt(x) == '-') {
                    isANum = true;
                }
            } else { // subsequent characters
                isANum = Character.isDigit(text.charAt(x));
            }
        }

        // If the new text passed the test
        if (isANum) {
            //Insert the new text into the oldValue
            String newValue = oldValue.substring(0, start) + text + oldValue.substring(end, oldValue.length());
            LOG.debug("newValue: " + newValue);
            //super.replaceText(start, end, text);
            LOG.debug("Just replaceText");
            //String newText = super.getText();
            boolean isANum2 = false;
            // Loop through the newValue
            for (int x = 0; x < newValue.length(); ++x) {
                if (x == 0) { // First character
                    if (Character.isDigit(newValue.charAt(x)) || newValue.charAt(x) == '-') {
                        isANum2 = true;
                    }
                } else { // Subsequent characters
                    if (Character.isDigit(newValue.charAt(x))) {
                        isANum2 = true;
                    } else {
                        isANum2 = false;
                        break;
                    }
                }
            }

            // If bad then set the oldValue
            if (!isANum2) {
                LOG.debug("isANum2 is false");
                super.setText(oldValue);
            } else { // Good so set the newValue
                LOG.debug("isANum2 is true");
                super.setText(newValue);
            }

        } else {
            super.setText(oldValue);
        }
    }

    @Override
    public void replaceSelection(String text) {
        LOG.debug("replaceSelection");
        String oldValue = getText();
        if (validate(text)) {
            super.replaceSelection(text);
            String newText = super.getText();
            if (newText != null && !validate(newText)) {
                super.setText(oldValue);
            }
        }
    }

    private boolean validate(String text) {
        return ("-1".equals(text) || text.matches(numberRegEx));
    }
}
