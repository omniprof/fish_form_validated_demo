package com.kenfogel.fish_form_validated_demo.beans;

import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * JavaFX bean for fish
 *
 * Added final to parameters
 *
 * @author Ken
 * @version 1.7
 *
 */
public class FishData {

    private IntegerProperty id;
    private StringProperty commonName;
    private StringProperty latin;
    private StringProperty ph;
    private StringProperty kh;
    private StringProperty temp;
    private StringProperty fishSize;
    private StringProperty speciesOrigin;
    private StringProperty tankSize;
    private StringProperty stocking;
    private StringProperty diet;

    /**
     * Non-default constructor
     *
     * @param id
     * @param commonName
     * @param latin
     * @param ph
     * @param kh
     * @param temp
     * @param fishSize
     * @param speciesOrigin
     * @param tankSize
     * @param stocking
     * @param diet
     */
    public FishData(final int id, final String commonName, final String latin, final String ph,
            final String kh, final String temp, final String fishSize, final String speciesOrigin,
            final String tankSize, final String stocking, final String diet) {
        super();
        this.id = new SimpleIntegerProperty(id);
        this.commonName = new SimpleStringProperty(commonName);
        this.latin = new SimpleStringProperty(latin);
        this.ph = new SimpleStringProperty(ph);
        this.kh = new SimpleStringProperty(kh);
        this.temp = new SimpleStringProperty(temp);
        this.fishSize = new SimpleStringProperty(fishSize);
        this.speciesOrigin = new SimpleStringProperty(speciesOrigin);
        this.tankSize = new SimpleStringProperty(tankSize);
        this.stocking = new SimpleStringProperty(stocking);
        this.diet = new SimpleStringProperty(diet);
    }

    /**
     * Default Constructor
     */
    public FishData() {
        this(-1, "", "", "", "", "", "", "", "", "", "");
    }

    public int getId() {
        return id.get();
    }

    public void setId(final int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getCommonName() {
        return commonName.get();
    }

    public void setCommonName(final String commonName) {
        this.commonName.set(commonName);
    }

    public StringProperty commonNameProperty() {
        return commonName;
    }

    public String getLatin() {
        return latin.get();
    }

    public void setLatin(final String latin) {
        this.latin.set(latin);
    }

    public StringProperty latinProperty() {
        return latin;
    }

    public String getPh() {
        return ph.get();
    }

    public void setPh(final String ph) {
        this.ph.set(ph);
    }

    public StringProperty phProperty() {
        return ph;
    }

    public String getKh() {
        return kh.get();
    }

    public void setKh(final String kh) {
        this.kh.set(kh);
    }

    public StringProperty khProperty() {
        return kh;
    }

    public String getTemp() {
        return temp.get();
    }

    public void setTemp(final String temp) {
        this.temp.set(temp);
    }

    public StringProperty tempProperty() {
        return temp;
    }

    public String getFishSize() {
        return fishSize.get();
    }

    public void setFishSize(final String size) {
        this.fishSize.set(size);
    }

    public StringProperty fishSizeProperty() {
        return fishSize;
    }

    public String getSpeciesOrigin() {
        return speciesOrigin.get();
    }

    public void setSpeciesOrigin(final String speciesOrigin) {
        this.speciesOrigin.set(speciesOrigin);
    }

    public StringProperty speciesOriginProperty() {
        return speciesOrigin;
    }

    public String getTankSize() {
        return tankSize.get();
    }

    public void setTankSize(final String tankSize) {
        this.tankSize.set(tankSize);
    }

    public StringProperty tankSizeProperty() {
        return tankSize;
    }

    public String getStocking() {
        return stocking.get();
    }

    public void setStocking(final String stocking) {
        this.stocking.set(stocking);
    }

    public StringProperty stockingProperty() {
        return stocking;
    }

    public String getDiet() {
        return diet.get();
    }

    public void setDiet(final String diet) {
        this.diet.set(diet);
    }

    public StringProperty dietProperty() {
        return diet;
    }

    @Override
    public String toString() {
        String s = "\n            ID = " + id.get() + "\n" + "   Common Name = "
                + commonName.get() + "\n" + "         Latin = " + latin.get() + "\n"
                + "            ph = " + ph.get() + "\n" + "            kh = " + kh.get()
                + "\n" + "          Temp = " + temp.get() + "\n"
                + "          Size = " + fishSize.get() + "\n" + "Species Origin = "
                + speciesOrigin.get() + "\n" + "     Tank Size = " + tankSize.get() + "\n"
                + "      Stocking = " + stocking.get() + "\n" + "          Diet = "
                + diet.get() + "\n";

        return s;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id.get());
        hash = 53 * hash + Objects.hashCode(this.commonName.get());
        hash = 53 * hash + Objects.hashCode(this.latin.get());
        hash = 53 * hash + Objects.hashCode(this.ph.get());
        hash = 53 * hash + Objects.hashCode(this.kh.get());
        hash = 53 * hash + Objects.hashCode(this.temp.get());
        hash = 53 * hash + Objects.hashCode(this.fishSize.get());
        hash = 53 * hash + Objects.hashCode(this.speciesOrigin.get());
        hash = 53 * hash + Objects.hashCode(this.tankSize.get());
        hash = 53 * hash + Objects.hashCode(this.stocking.get());
        hash = 53 * hash + Objects.hashCode(this.diet.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FishData other = (FishData) obj;
        if (!Objects.equals(this.id.get(), other.id.get())) {
            return false;
        }
        if (!Objects.equals(this.commonName.get(), other.commonName.get())) {
            return false;
        }
        if (!Objects.equals(this.latin.get(), other.latin.get())) {
            return false;
        }
        if (!Objects.equals(this.ph.get(), other.ph.get())) {
            return false;
        }
        if (!Objects.equals(this.kh.get(), other.kh.get())) {
            return false;
        }
        if (!Objects.equals(this.temp.get(), other.temp.get())) {
            return false;
        }
        if (!Objects.equals(this.fishSize.get(), other.fishSize.get())) {
            return false;
        }
        if (!Objects.equals(this.speciesOrigin.get(), other.speciesOrigin.get())) {
            return false;
        }
        if (!Objects.equals(this.tankSize.get(), other.tankSize.get())) {
            return false;
        }
        if (!Objects.equals(this.stocking.get(), other.stocking.get())) {
            return false;
        }
        if (!Objects.equals(this.diet.get(), other.diet.get())) {
            return false;
        }
        return true;
    }

}
